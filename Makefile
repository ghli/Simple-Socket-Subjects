CC=gcc
EXT=.c
BIN=bin
SRC=src
INC=include
TARGETS=$(patsubst ${SRC}/%${EXT},${BIN}/%, $(shell find ${SRC} -name '*.c'))
CFLAGS=-lpthread -I${INC} -g

.PHONY : all clean

all : ${TARGETS}

${BIN}/% : ${SRC}/%${EXT}
	${CC} -o$@ $< ${CFLAGS}

clean :
	rm ${TARGETS}
