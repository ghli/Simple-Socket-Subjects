#ifndef G_MSG_H
#define G_MSG_H
#define NAME_LEN 10
#define MSG_LEN 140
#include <string.h>
#define min(X, Y) ((X) < (Y) ? (X) : (Y))
struct msg {
    char name[NAME_LEN+1];
    char msg[MSG_LEN+1];
};

struct msg prep(char* name, char* msg) {
    struct msg t;
    memset(&t,'\0',sizeof(t));
    memcpy(t.name,name,min(NAME_LEN,strlen(name)));
    memcpy(t.msg,msg,min(MSG_LEN,strlen(msg)));
    return t;
}

void print(struct msg m) {
    if (strlen(m.name)==0||strlen(m.msg)==0)
        return;
    printf("%s:\n%s",m.name,m.msg);
}

#endif
