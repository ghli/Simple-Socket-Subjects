#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <pthread.h>
#include <argp.h>
#include "msg.h"

struct argp_option options[] = {
    {"port",    'p',    "PORT",  0,  "Port number of server"},
};

static char doc[] = "A simple server for the little chat client I made.";
static char args_doc[] = "ARG1 ARG2 ARG3";

struct arguments {
    unsigned short port;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;
    switch (key) {
        case 'p':
            sscanf(arg, "%hi", &(arguments->port));
        break;
        case ARGP_KEY_END:
            if (state->arg_num > 1) {
                printf("Too many arguments :^\\\n");
                exit(0);
            }
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc };

struct threadcounter {
    int n;
    pthread_mutex_t m;
};

struct tc {
    int fd;
    struct threadcounter* t;
};

void inc(struct threadcounter* t) {
    pthread_mutex_lock(&t->m);
    t->n++;
    pthread_mutex_unlock(&t->m);
}

void dec(struct threadcounter* t) {
    pthread_mutex_lock(&t->m);
    t->n--;
    pthread_mutex_unlock(&t->m);
}

void* handlemsg(void* in) {
    struct msg buf;
    struct tc* t = (struct tc*)in;
    inc(t->t);
    while(recv(t->fd, &buf, sizeof(buf), MSG_WAITALL) > 0) {
        print(buf);
    }
    close(t->fd);
    dec(t->t);

    free(t);
    return NULL;
}
int main(int argc, char **argv) {
    struct arguments args;
    args.port = 5000;
    argp_parse(&argp, argc, argv, 0, 0, &args);

    int ld = 0, cd = 0;

    struct threadcounter t;
    t.n = 0;
    pthread_mutex_init(&t.m,NULL);

    struct msg buf;

    
    ld = socket(AF_INET,SOCK_STREAM,0);
    printf("Socket Acquired");

    struct sockaddr_in serv_addr;
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(args.port);

    bind (ld, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    if(listen(ld, 10) == -1) {
        printf("Couldn't listen on port %d", args.port);
        return -1;
    }
    pthread_t ts[10];
    while(1) {
        cd = accept(ld, (struct sockaddr*)NULL, NULL);
        printf("Connection accepted.");
        struct tc* c = malloc(sizeof(struct tc));
        c->fd = cd;
        c->t = &t;
        pthread_create(&ts[t.n], NULL, &handlemsg, c);
        printf("%d\n",t.n);
    }
}
