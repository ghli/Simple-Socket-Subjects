#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <argp.h>
#include "msg.h"

struct argp_option options[] = {
    {"address", 'a',    "ADDRESS",  0,  "Address of server to connect to"},
    {"port",    'p',    "PORT",  0,  "Port number of server"},
    {"name",    'n',    "NAME",  0,  "Your name"}
};
static char doc[] = "A simple client for the little chat server I made.";
static char args_doc[] = "ARG1 ARG2 ARG3";
struct arguments {
    char addr[30];
    char port[6];
    char name[11];
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;
    switch (key) {
        case 'a':
            strncpy(arguments->addr,arg,29);
            arguments->addr[29]='\0';
            break;
        case 'p':
            strncpy(arguments->port,arg,5);
            arguments->port[5]='\0';
            break;
        case 'n':
            memcpy(arguments->name,arg,10);
            arguments->name[10]='\0';
            break;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc };

int main(int argc, char** argv) {

    struct arguments arguments;
    strncpy(arguments.port,"5000\0",6);
    strncpy(arguments.addr,"127.0.0.1\0",30);
    strncpy(arguments.name,"Anonymous\0",11);
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    struct addrinfo hints, *res;
    memset(&hints,0,sizeof hints);
    hints.ai_family=AF_UNSPEC;
    hints.ai_socktype=SOCK_STREAM;
    if (getaddrinfo(arguments.addr,arguments.port,&hints,&res)) {
        printf("Lookup failed for %s\n", arguments.addr);
        return 2;
    };

    int sd=0;
    if((sd=socket(res->ai_family, res->ai_socktype, res->ai_protocol))<0) {
        printf("Something's wrong, capt'n");
        return 1;
    }

    if (connect(sd,res->ai_addr,res->ai_addrlen)) {
        freeaddrinfo(res);
        printf("Oof'd again, capt'n\n%s",strerror(errno));
        return 1;
    }
    freeaddrinfo(res);

    struct msg buf;
    memset(&buf,'\0',sizeof(buf));

    char amsg[1000];
    size_t gay = 1000;
    char *msg = amsg;
    while(1) {
        memset(msg,'\0',1000);
        getline(&msg,&gay,stdin);
        buf = prep(arguments.name,msg);
        send(sd, &buf, sizeof(buf),0);
    }
}
